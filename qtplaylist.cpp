/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtplaylist.h"

#include "despotify_cpp.h"

QtPlaylist::QtPlaylist(despotify_session *session, QObject *parent)
    : QObject(parent), m_session(session), m_searchResult(0)
{
}

QtPlaylist::~QtPlaylist()
{
    cleanUp();
}

QString QtPlaylist::name() const
{
    switch (m_type) {
    case Playlist:
        if (m_playlist != 0)
            return QString::fromUtf8(m_playlist->name);
        break;
    case Search:
        if (m_searchResult != 0)
            return QString::fromUtf8(reinterpret_cast<char *>(m_searchResult->query));
        break;
    case Album:
        if (m_album != 0)
            return tr("Songs from %1 by %2").arg(QString::fromUtf8(m_album->name));
        break;
    case Artist:
        if (m_artist != 0)
            return tr("Songs by %1").arg(QString::fromUtf8(m_artist->name));
        break;
    default:
        break;
    }

    return tr("Empty list");
}

void QtPlaylist::cleanUp()
{
    switch (m_type) {
    case Playlist:
        if (m_playlist != 0) {
            // Can't free playlist, as only the root playlist can have an owner
            // Main window owns root playlist
            m_playlist = 0;
        }
        break;
    case Search:
        if (m_searchResult != 0) {
            despotify_free_search(m_searchResult);
            m_searchResult = 0;
        }
        break;
    case Album:
        if (m_album != 0) {
            despotify_free_album_browse(m_album);
            m_album = 0;
        }
        break;
    case Artist:
        if (m_artist != 0) {
            despotify_free_artist_browse(m_artist);
            m_artist = 0;
        }
        break;
    default:
        break;
    }
}

QList<track *> QtPlaylist::makeList(track *t) const
{
    QList<track *> list;

    while (t != 0) {
        list.append(t);
        t = t->next;
    }

    return list;
}

void QtPlaylist::setPlaylist(playlist *pl)
{
    cleanUp();

    m_type = Playlist;
    m_playlist = pl;
}

void QtPlaylist::setSearchTerm(const QString &searchTerm)
{
    cleanUp();

    m_type = Search;
    m_searchResult = despotify_search(m_session, searchTerm.toUtf8().data(), 50);
}

void QtPlaylist::searchMore()
{
    if (m_type == Search && m_searchResult != 0)
        despotify_search_more(m_session, m_searchResult, m_searchResult->total_tracks, 50);
}

void QtPlaylist::setAlbum(const QByteArray &albumId)
{
    cleanUp();

    m_type = Album;
    m_album = despotify_get_album(m_session, QByteArray(albumId).data());
}

void QtPlaylist::setArtist(const QByteArray &artistId)
{
    cleanUp();

    m_type = Artist;
    m_artist = despotify_get_artist(m_session, QByteArray(artistId).data());
}

QList<track *> QtPlaylist::tracks() const
{
    switch (m_type) {
    case Playlist:
        if (m_playlist != 0)
            return makeList(m_playlist->tracks);
        break;
    case Search:
        if (m_searchResult != 0)
            return makeList(m_searchResult->tracks);
        break;
    case Artist:
        if (m_artist != 0) {
            QList<track *> trackList;

            album_browse *album = m_artist->albums;
            while (album != 0) {
                trackList += makeList(album->tracks);
                album = album->next;
            }

            return trackList;
        }
        break;
    case Album:
        if (m_album != 0)
            return makeList(m_album->tracks);
        break;
    default:
        break;
    }

    return QList<track*>();
}
