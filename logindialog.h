/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>

namespace Ui {
    class LogInDialog;
}

class LogInDialog : public QDialog {
    Q_OBJECT
public:
    LogInDialog(QWidget *parent = 0);
    ~LogInDialog();

    QString userName() const;
    QString password() const;
    bool rememberSettings() const;

    void setUserName(const QString &userName);
    void setPassword(const QString &password);
    void setRememberSettings(bool on);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::LogInDialog *ui;
};

#endif // LOGINDIALOG_H
