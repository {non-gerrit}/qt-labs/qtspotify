#ifndef AUDIOOUTPUT_H
#define AUDIOOUTPUT_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include <alsa/asoundlib.h>

#include <despotify_cpp.h>

class AudioOutput : public QThread
{
public:
    AudioOutput();
    ~AudioOutput();
    void setDespotifySession(despotify_session *ds);
    void play();
    void pause();
    void flush();

protected:
    enum State { Play, Pause };
    void run();

private:
    QWaitCondition m_isPlaying;
    QWaitCondition m_isPause;
    snd_pcm_t *m_alsaHandle;
    despotify_session *m_despotifySession;

    //is going to be 16 * 2 / 8
    //bitwidth * channels
    int frameSize;

    uint m_channels;
    uint m_bitrate;

    State m_state;
};

#endif // AUDIOOUTPUT_H
