/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef STOREDPLAYLISTMODEL_H
#define STOREDPLAYLISTMODEL_H

#include <QAbstractListModel>

struct playlist;
class QtPlaylist;
struct despotify_session;
class StoredPlaylistModel: public QAbstractListModel
{
    Q_OBJECT
public:
    StoredPlaylistModel(despotify_session *session, playlist *rootPlaylist, QObject *parent = 0);
    ~StoredPlaylistModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

private:
    despotify_session *m_session;
    playlist *m_rootPlaylist;

    QList<QtPlaylist *> m_playlists;
};

#endif // STOREDPLAYLISTMODEL_H
