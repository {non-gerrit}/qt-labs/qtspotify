/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "searchmodel.h"
#include "qtplaylist.h"

SearchModel::SearchModel(despotify_session *session, QObject *parent)
    : QAbstractListModel(parent), m_session(session)
{
}

int SearchModel::rowCount(const QModelIndex &parent) const
{
    Q_ASSERT(!parent.isValid());
    return m_playlists.size();
}

QVariant SearchModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.column() == 0);
    if (role == Qt::DisplayRole)
        return m_playlists.at(index.row())->name();
    else if (role == Qt::UserRole)
        return QVariant::fromValue(m_playlists.at(index.row()));

    return QVariant();
}

void SearchModel::addSearch(const QString &searchTerm)
{
    beginInsertRows(QModelIndex(), m_playlists.size(), m_playlists.size());

    QtPlaylist *playlist = new QtPlaylist(m_session, this);
    playlist->setSearchTerm(searchTerm);
    m_playlists.append(playlist);

    endInsertRows();

    emit searchAdded(searchTerm);
}

