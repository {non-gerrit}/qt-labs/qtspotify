/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

/*
    Logic paraphrased from Andreas Kling's repository at

    http://qt.gitorious.org/~andreaskling/qt-labs/andreasklings-qtspotify
*/

#include "coverdatabase.h"
#include "despotify_cpp.h"

#include <QtCore/QFuture>
#include <QtCore/QtConcurrentRun>
#include <QtCore/QFutureWatcher>
#include <QtGui/QPixmap>

CoverDatabase::CoverDatabase(despotify_session *session, QObject *parent)
    : QObject(parent), m_session(session)
{
}

CoverDatabase::~CoverDatabase()
{
}

void CoverDatabase::loadCover(const QByteArray &coverId)
{
    Q_ASSERT(m_session != 0);

    m_currentCoverId = coverId;
    if (m_pendingCovers.contains(coverId))
        return;

    {
        QMutexLocker locker(&m_coverCacheLock);
        QImage img = m_coverCache.value(coverId);

        if (!img.isNull()) {
            emit coverLoaded(QPixmap::fromImage(img));
            return;
        }
    }

    QFuture<QByteArray> imageDataFetcher = QtConcurrent::run(this, &CoverDatabase::fetchImageData,
                                                             coverId);
    QFutureWatcher<QByteArray> *imageDataFetcherWatcher = new QFutureWatcher<QByteArray>(this);
    connect(imageDataFetcherWatcher, SIGNAL(finished()), this, SLOT(imageDataFetched()));
    imageDataFetcherWatcher->setFuture(imageDataFetcher);
}

QByteArray CoverDatabase::fetchImageData(const QByteArray &coverId)
{
    Q_ASSERT(m_session != 0);

    void *data;
    int len;

    {
        /* despotify doesn't like having multiple get_image calls running */
        static QMutex despotify_get_image_mutex;
        QMutexLocker locker(&despotify_get_image_mutex);
        data = despotify_get_image(m_session, const_cast<char *>(coverId.data()), &len);
    }

    QImage img = data != 0 ? QImage::fromData(static_cast<const uchar *>(data), len, "JPEG") : QImage();
    free(data);

    {
        QMutexLocker locker(&m_coverCacheLock);
        m_coverCache.insert(coverId, img);
    }

    return coverId;
}

void CoverDatabase::imageDataFetched()
{
    QFutureWatcher<QByteArray> *watcher = static_cast<QFutureWatcher<QByteArray> *>(sender());
    Q_ASSERT(watcher != 0);

    QByteArray coverId = watcher->result();
    m_pendingCovers.remove(coverId);
    {
        QMutexLocker locker(&m_coverCacheLock);
        if (coverId == m_currentCoverId)
            emit coverLoaded(QPixmap::fromImage(m_coverCache.value(m_currentCoverId)));
    }

}
