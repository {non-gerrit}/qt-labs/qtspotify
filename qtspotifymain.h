/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef QTSPOTIFYMAIN_H
#define QTSPOTIFYMAIN_H

#include "ui_qtspotify.h"
#include "audiooutput.h"

#include <QtGui/QMainWindow>

#include <QtCore/QFuture>
#include <QtCore/QFutureWatcher>

class MyStatusBar: public QStatusBar
{
    Q_OBJECT
    Q_PROPERTY(QString statusMessage READ statusMessage WRITE setStatusMessage);
public:
    MyStatusBar(QWidget *parent = 0) : QStatusBar(parent)
    {
        m_label = new QLabel();
        addPermanentWidget(m_label);
    }

    void setStatusMessage(const QString &text)
    {
        m_label->setText(text);
    }

    QString statusMessage() const
    {
        return m_label->text();
    }

private:
    QLabel *m_label;
};

struct despotify_session;
struct track;
struct playlist;
struct search_result;
class QtPlaylist;

class QStateMachine;
class QState;

class LogInDialog;
class CoverDatabase;
class QtSpotifyMain: public QMainWindow
{
    Q_OBJECT
    Q_PROPERTY(QString debuggingMessage READ debuggingMessage WRITE debug)
public:
    QtSpotifyMain(QWidget *parent = 0);
    ~QtSpotifyMain();


    // Just a dummy so we can use the property mechanism to call the debug() function
    QString debuggingMessage() const { return QString(); }

    bool debugging() const { return m_debugging; }

protected:
    bool event(QEvent *event);

private slots:
    void logIn();
    void endSession();
    void retrievePlayLists();
    void populateSearchBox();
    void play();
    void pause();
    void stop();
    void resume();
    void selectPlayList();
    void selectSearch();
    void decideLoginResult();    
    void debug(const QString &text);
    void setDebugging(bool on) { m_debugging = on; }
    void search();    
    void skip();
    void previous();
    void searchMore();
    void playlistActivated(const QModelIndex &index);

signals:
    void loggedIn();
    void loginFailed();
    void searched();
    void playlistItemActivated();

private:
    void setPlaylist(QList<track *> tracks);

    void initUi();
    void initWatchers();

    void initMachine();
    void initLoggingInState(QState *);
    void initLoggedInState(QState *);
    void initPlayListHandlingState(QState *);
    void initPlayBackHandlingState(QState *);
    void initPlayingState(QState *);
    void initIdleState(QState *);
    void setNewTrack(track *t);
    void selectTrack(track *t);
    void selectSearchOrPlaylist(QListView *listView);

    Ui_QtSpotifyMain m_ui;
    despotify_session *m_session;

    playlist *m_rootPlaylist;
    QStateMachine *m_machine;
    CoverDatabase *m_coverDatabase;

    QList<QtPlaylist *> m_searches;

    bool m_debugging;

    QFutureWatcher<bool> *m_authenticationWatcher;
    QFutureWatcher<playlist *> *m_retrievingPlayListWatcher;

    LogInDialog *m_logInDialog;

    AudioOutput m_audio;
};

#endif // QTSPOTIFYMAIN_H
