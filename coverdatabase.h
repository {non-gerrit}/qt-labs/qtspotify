/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifndef COVERDATABASE_H
#define COVERDATABASE_H

#include <QtCore/QObject>
#include <QtCore/QHash>
#include <QtCore/QMutex>
#include <QtCore/QSet>

#include <QtGui/QImage>

class QLabel;
struct despotify_session;
class CoverDatabase: public QObject
{
   Q_OBJECT
public:
    CoverDatabase(despotify_session *session, QObject *parent = 0);
    ~CoverDatabase();

    void loadCover(const QByteArray &coverId);

private slots:
    void imageDataFetched();

signals:
    void coverLoaded(const QPixmap &pm);

private:
    QByteArray fetchImageData(const QByteArray &coverId);

    despotify_session *m_session;
    QByteArray m_currentCoverId;
    QSet<QByteArray> m_pendingCovers;
    QHash<QByteArray, QImage> m_coverCache;
    QMutex m_coverCacheLock;
};

#endif // COVERDATABASE_H
