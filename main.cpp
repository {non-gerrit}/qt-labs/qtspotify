/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "qtspotifymain.h"

#include "despotify_cpp.h"

int main(int argc, char **argv)
{
    if (!despotify_init()) {
        qWarning("Couldn't initialize despotify");
        return 1;
    }

    QApplication app(argc, argv);

    QtSpotifyMain qsm;
    qsm.showMaximized();

    int ret = app.exec();

    if (!despotify_cleanup()) {
        qWarning("Couldn't clean up despotify");
        return 2;
    }

    return ret;
}

