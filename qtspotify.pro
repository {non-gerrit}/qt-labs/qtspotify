HEADERS += qtspotifymain.h \
    logindialog.h \
    despotify_cpp.h \
    qtplaylist.h \
    storedplaylistmodel.h \
    coverdatabase.h \
    searchdialog.h \
    searchmodel.h \
    audiooutput.h
SOURCES += qtspotifymain.cpp \
    main.cpp \
    logindialog.cpp \
    qtplaylist.cpp \
    storedplaylistmodel.cpp \
    coverdatabase.cpp \
    searchdialog.cpp \
    searchmodel.cpp \
    audiooutput.cpp
FORMS += qtspotify.ui \
    logindialog.ui \
    searchdialog.ui
LIBS += -ldespotify -lasound
